package day01;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Demo1 {

	public static void main(String[] args) {
		Connection con = null;
		String url = "jdbc:mysql://localhost:3306/fsd60";
		
		try {
		
			//1. Loading the MySQL Driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			
			//2. Establishing the Connection
			con = DriverManager.getConnection(url, "root", "root");
			
			if (con != null) {
				System.out.println("Successfully Established the Connection");
				
				con.close();
				
			} else {
				System.out.println("Failed to Establish the Connection");
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
}


**********************************************************
***************************************
package day01;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import com.db.DbConnection;

public class Demo02 {
	public static void main(String[] args) {
		
		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		
		int empId = 107;
		String empName = "Utkarsh";
		double salary = 9898.98;
		String gender = "Male";
		String emailId = "utkarsh@gmail.com";
		String password = "123";
		
		String insertQuery = "insert into employee values (" + 
				empId + ", '" + empName + "', " + salary + ", '" + 
				gender + "', '" + emailId + "', '" + password + "')";
				
		try {
			statement = connection.createStatement();
			int result = statement.executeUpdate(insertQuery);
			
			if (result > 0) {
				System.out.println(result + " Record(s) Inserted...");
			} else {
				System.out.println("Record Insertion Failed...");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			if (connection != null) {
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}

************************************
**********************************
package day01;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;

///Update Utkarsh Record and update salary to 25000
public class Demo3 {
	public static void main(String[] args) {
		
		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter EmployeeId and New Salary");
		int empId = scanner.nextInt();
		double salary = scanner.nextDouble();
		
		String updateQuery = "update employee set salary = " + salary + " where empId = " + empId;
				
		try {
			statement = connection.createStatement();
			int result = statement.executeUpdate(updateQuery);
			
			if (result > 0) {
				System.out.println(result + " Record(s) Updated...");
			} else {
				System.out.println("Record Updation Failed...");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			if (connection != null) {
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}
*****************************************************
***************************************************
package day01;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;

///Delete Utkarsh Record from Employee Table
public class Demo4 {
	public static void main(String[] args) {
		
		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter EmployeeId: ");
		int empId = scanner.nextInt();
		
		String deleteQuery = "delete from employee where empId = " + empId;
				
		try {
			statement = connection.createStatement();
			int result = statement.executeUpdate(deleteQuery);
			
			if (result > 0) {
				System.out.println(result + " Record(s) Deleted...");
			} else {
				System.out.println("Record Deletion Failed...");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			if (connection != null) {
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}

*********************************************************
*********************************************************
package day01;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.db.DbConnection;

//Get All Records from Employee Table
public class Demo5 {
	public static void main(String[] args) {

		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		ResultSet resultSet = null;

		String selectQuery = "select * from employee";

		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(selectQuery);

			if (resultSet != null) {

				while(resultSet.next()) {

//					System.out.println("EmpId   : " + resultSet.getInt(1));
//					System.out.println("EmpName : " + resultSet.getString(2));
//					System.out.println("Salary  : " + resultSet.getDouble("salary"));
//					System.out.println("Gender  : " + resultSet.getString(4));
//					System.out.println("EmailId : " + resultSet.getString(5));
//					System.out.println("Password: " + resultSet.getString(6));
//					System.out.println();
					
					System.out.print(resultSet.getInt(1)    + " " + resultSet.getString(2) + " ");
					System.out.print(resultSet.getDouble(3) + " " + resultSet.getString(4) + " ");
					System.out.print(resultSet.getString(5) + " " + resultSet.getString(6) + " ");
					System.out.println("\n");
				}

			} else {
				System.out.println("No Record(s) Found!!!");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			if (connection != null) {
				resultSet.close();
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
}


