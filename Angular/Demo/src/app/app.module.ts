import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { Test2Component } from './test2/test2.component';
import { LoginComponent } from './login/login.component';
import { ShowemployeesComponent } from './showemployees/showemployees.component';
import { ExpPipe } from './exp.pipe';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { ProductComponent } from './product/product.component';
import { ShowempbyidComponent } from './showempbyid/showempbyid.component';
import { LogoutComponent } from './logout/logout.component';
import { RegisterComponent } from './register/register.component';
import { GenderPipe } from './gender.pipe';
//For HTTP Client Requests
import { HttpClientModule } from '@angular/common/http';
import { CartComponent } from './cart/cart.component';

@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    Test2Component,
    LoginComponent,
    ShowemployeesComponent,
    ExpPipe,
    GenderPipe,
    HeaderComponent,
    ProductComponent,
    ShowempbyidComponent,
    LogoutComponent,
    RegisterComponent,
    CartComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule,
    HttpClientModule  //For HTTP Client Requests
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }










// import { NgModule } from '@angular/core';
// import { RouterModule, Routes } from '@angular/router';
// import { LoginComponent } from './login/login.component';
// import { RegisterComponent } from './register/register.component';
// import { ShowemployeesComponent } from './showemployees/showemployees.component';
// import { ShowempbyidComponent } from './showempbyid/showempbyid.component';
// import { ProductComponent } from './product/product.component';
// import { LogoutComponent } from './logout/logout.component';
// import { authGuard } from './auth.guard';
// import { CartComponent } from './cart/cart.component';

// const routes: Routes = [
//   {path:'',            component:LoginComponent},
//   {path:'login',       component:LoginComponent},
//   {path:'register',    component:RegisterComponent},
//   {path:'showemps',    canActivate:[authGuard],component:ShowemployeesComponent},
//   {path:'showempbyid', canActivate:[authGuard],component:ShowempbyidComponent},
//   {path:'products',    canActivate:[authGuard],component:ProductComponent},
//   {path:'logout',      canActivate:[authGuard],component:LogoutComponent },
//   {path:'cart',        canActivate: [authGuard],component: CartComponent}
// ];

// @NgModule({
//   imports: [RouterModule.forRoot(routes)],
//   exports: [RouterModule]
// })
// export class AppRoutingModule { }
