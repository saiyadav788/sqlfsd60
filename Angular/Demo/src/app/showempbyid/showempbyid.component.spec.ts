import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowempbyidComponent } from './showempbyid.component';

describe('ShowempbyidComponent', () => {
  let component: ShowempbyidComponent;
  let fixture: ComponentFixture<ShowempbyidComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ShowempbyidComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ShowempbyidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
